// Script to identify where pointer members are repeatedly dereferenced (doesn't handle loops or calls well)

import com.microsoft.z3.*;
import docking.ComponentProvider;
import docking.widgets.table.AbstractGTableModel;
import ghidra.app.plugin.processors.sleigh.SleighLanguage;
import ghidra.app.script.GhidraScript;
import ghidra.program.model.address.Address;
import ghidra.program.model.address.AddressSet;
import ghidra.program.model.listing.Function;
import ghidra.program.model.pcode.HighVariable;
import ghidra.program.model.pcode.PcodeOp;
import ghidra.program.model.pcode.PcodeOpAST;
import ghidra.program.model.pcode.Varnode;
import ghidra.util.table.GhidraTable;
import ghidra.util.table.GhidraTableFilterPanel;
import info.addisoncrump.schadenfreude.constraint.PcodeTheoremConverter;
import info.addisoncrump.schadenfreude.decomp.SchadenfreudeDecompiler;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.Queue;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.microsoft.z3.Status.SATISFIABLE;
import static com.microsoft.z3.Status.UNSATISFIABLE;

public class FindTOCTOU extends GhidraScript {
    @Override
    protected void run() throws Exception {
        if (currentLocation == null) {
            println("No region selected; failing fast.");
            return;
        }

        var func = currentProgram.getFunctionManager().getFunctionContaining(currentLocation.getAddress());
        var ctx = new Context();
        var solver = ctx.mkSolver(ctx.mkTactic("qfbv"));

        var decomp = SchadenfreudeDecompiler.getInstance();
        var res = decomp.decompileFunction(func, 10, monitor).get(); // this is cached

        var loads = stream(res.getHighFunction().getPcodeOps())
                .filter(op -> op.getOpcode() == PcodeOp.LOAD)
                .collect(Collectors.toList());

        var searched = new HashMap<PcodeOpAST, MultiValuedMap<Long, HighVariable>>();
        var memory = ctx.mkConst("vmem", ctx.mkArraySort(ctx.mkBitVecSort(currentProgram.getDefaultPointerSize() * 8),
                ctx.mkBitVecSort(((SleighLanguage) currentProgram.getLanguage()).getDefaultPointerWordSize() * 8)));

        for (var load : loads) {
            searched.put(load, searchSharedPointers(ctx, solver, memory, load));
        }

        var formals = loads.stream()
                .map(op -> PcodeTheoremConverter.extract(ctx, op.getInput(1)))
                .collect(Collectors.toList());

        if (solver.check() != SATISFIABLE) {
            println("Couldn't compute.");
            return;
        }
        var model = solver.getModel();

        List<ReusedPointer> reuses = new ArrayList<>();
        for (int i = 0; i < loads.size() - 1; i++) {
            for (int j = i + 1; j < loads.size(); j++) {
                if (loads.get(i).getInput(1).getHigh().getDataType().isEquivalent(loads.get(j).getInput(1).getHigh().getDataType())) {
                    var map1 = searched.get(loads.get(i));
                    var map2 = searched.get(loads.get(j));
                    MultiValuedMap<Long, HighVariable> merged = new ArrayListValuedHashMap<>();
                    for (var entry1 : map1.entries()) {
                        for (var entry2 : map2.entries()) {
                            if (entry1.getValue() == entry2.getValue()) {
                                merged.put(entry1.getKey() * entry2.getKey(), entry1.getValue());
                            }
                        }
                    }
                    var highest = merged.entries().stream().max(Comparator.comparingLong(Map.Entry::getKey));
                    if (highest.isPresent() && solver.check(ctx.mkNot(ctx.mkEq(formals.get(i), formals.get(j)))) == UNSATISFIABLE) {
                        var best = highest.get().getValue();
                        Expr<IntSort> offset = model.evaluate(ctx.mkBV2Int(ctx.mkBVSub(formals.get(i), PcodeTheoremConverter.extract(ctx, best.getRepresentative())), true), true);
                        reuses.add(new ReusedPointer(highest.get().getValue(), Long.parseLong(offset.toString()), loads.get(i).getSeqnum().getTarget(), loads.get(j).getSeqnum().getTarget()));
                    }
                }
            }
        }

        if (reuses.isEmpty()) {
            this.popup("No pointer reuses found.");
            return;
        }

        var tableModel = new PointerReuseTableModel(func, reuses);
        var window = new PointerReuseWindow(tableModel);
        state.getTool().addComponentProvider(window, true);
    }

    private MultiValuedMap<Long, HighVariable> searchSharedPointers(Context ctx,
                                                               Solver solver,
                                                               Expr<ArraySort<BitVecSort, BitVecSort>> memory,
                                                               PcodeOpAST load) {
        Set<Varnode> visited = new HashSet<>();
        Queue<Varnode> currQueue = new LinkedList<>();
        Queue<Varnode> nextQueue = new LinkedList<>();
        currQueue.add(load.getInput(1));
        MultiValuedMap<Long, HighVariable> discovered = new ArrayListValuedHashMap<>();

        for (long i = 0; !currQueue.isEmpty(); i++) {
            while (!currQueue.isEmpty()) {
                var curr = currQueue.poll();
                if (visited.contains(curr)) {
                    continue;
                }
                visited.add(curr);

                var def = curr.getDef();
                if (def != null) {
                    Collections.addAll(nextQueue, def.getInputs());
                    var op = PcodeTheoremConverter.convert(memory, ctx, def);
                    if (op.getAssertion() != null) {
                        solver.add(op.getAssertion());
                    }
                }
                var high = curr.getHigh();
                if (high != null && high.getName() != null) {
                    discovered.put(i, high);
                }
            }
            currQueue = nextQueue;
            nextQueue = new LinkedList<>();
        }

        return discovered;
    }

    private <T> Stream<T> stream(Iterator<T> iter) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iter, Spliterator.NONNULL), false);
    }

    private static class ReusedPointer {
        private final HighVariable highest;
        private final long offset;
        private final Address first;
        private final Address second;


        private ReusedPointer(HighVariable highest, long offset, Address first, Address second) {
            this.highest = highest;
            this.offset = offset;
            if (first.subtract(second) < 0) {
                this.first = first;
                this.second = second;
            } else {
                this.first = second;
                this.second = first;
            }
        }
    }

    private static class PointerReuseTableModel extends AbstractGTableModel<ReusedPointer> {

        private final Function func;
        private final List<ReusedPointer> pointers;

        public PointerReuseTableModel(Function func, List<ReusedPointer> pointers) {
            this.func = func;
            this.pointers = pointers;
        }

        @Override
        public String getName() {
            return "Reused pointers - " + func.getName(true);
        }

        @Override
        public List<ReusedPointer> getModelData() {
            return pointers;
        }

        @Override
        public Object getColumnValueForRow(ReusedPointer reusedPointer, int columnIndex) {
            return switch (columnIndex) {
                case 0 -> reusedPointer.highest.getName();
                case 1 -> reusedPointer.offset;
                case 2 -> reusedPointer.first;
                case 3 -> reusedPointer.second;
                default -> null;
            };
        }

        @Override
        public int getColumnCount() {
            return 4;
        }

        @Override
        public String getColumnName(int column) {
            return switch (column) {
                case 0 -> "Base Pointer";
                case 1 -> "Offset";
                case 2 -> "First Access";
                case 3 -> "Second Access";
                default -> null;
            };
        }
    }

    private class PointerReuseWindow extends ComponentProvider {

        private GhidraTable table;
        private GhidraTableFilterPanel<ReusedPointer> filterPanel;
        private final JComponent component;

        public PointerReuseWindow(PointerReuseTableModel tableModel) {
            super(state.getTool(), tableModel.getName(), "TOCTOU Scanner");
            component = createWorkPanel(tableModel);
        }

        private JComponent createWorkPanel(PointerReuseTableModel model) {
            JPanel workPanel = new JPanel(new BorderLayout());

            table = new GhidraTable(model);
            table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

            table.getSelectionModel().addListSelectionListener(e -> {
                if (e.getValueIsAdjusting() || table.getSelectedRow() == -1) {
                    return;
                }

                var first = (Address) table.getValueAt(table.getSelectedRow(), 2);
                var second = (Address) table.getValueAt(table.getSelectedRow(), 3);

                var view = new AddressSet();
                view.add(first);
                view.add(second);
                setCurrentLocation(first);
                setCurrentSelection(view);
            });

            filterPanel = new GhidraTableFilterPanel<>(table, model);
            workPanel.add(new JScrollPane(table), BorderLayout.CENTER);
            workPanel.add(filterPanel, BorderLayout.SOUTH);
            return workPanel;
        }

        @Override
        public JComponent getComponent() {
            return component;
        }

        @Override
        public void closeComponent() {
            super.closeComponent();
            table.dispose();
            filterPanel.dispose();
        }
    }
}
