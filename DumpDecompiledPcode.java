import ghidra.app.script.GhidraScript;

import ghidra.program.model.pcode.PcodeOpAST;
import info.addisoncrump.schadenfreude.decomp.SchadenfreudeDecompiler;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class DumpDecompiledPcode extends GhidraScript {
    @Override
    protected void run() throws Exception {
        if (currentLocation == null) {
            return;
        }

        var func = currentProgram.getFunctionManager().getFunctionContaining(currentLocation.getAddress());

        var decomp = SchadenfreudeDecompiler.getInstance();

        var res = decomp.decompileFunction(func, 10, getMonitor()).get();
        if (res.decompileCompleted()) {
            stream(res.getHighFunction().getPcodeOps()).map(PcodeOpAST::toString).forEach(this::println);
        }
    }

    private <T> Stream<T> stream(Iterator<T> iter) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iter, Spliterator.NONNULL), false);
    }
}
