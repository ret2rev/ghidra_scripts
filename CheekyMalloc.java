// Searches for cheeky malloc calls with naughty operations as an argument.

import ghidra.app.decompiler.DecompileResults;
import ghidra.app.decompiler.parallel.DecompileConfigurer;
import ghidra.app.decompiler.parallel.DecompilerCallback;
import ghidra.app.decompiler.parallel.ParallelDecompiler;
import ghidra.app.script.GhidraScript;
import ghidra.program.model.address.Address;
import ghidra.program.model.listing.Function;
import ghidra.program.model.listing.Program;
import ghidra.program.model.pcode.PcodeOp;
import ghidra.program.model.pcode.Varnode;
import ghidra.program.model.symbol.Reference;
import ghidra.util.task.TaskMonitor;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class CheekyMalloc extends GhidraScript {

    public void run() throws Exception {
        boolean filter = true;
        if (currentSelection == null || currentSelection.isEmpty()) {
            if (isRunningHeadless()) {
                filter = false; // disable address filtering
            } else {
                return; // we don't know what we're looking for
            }
        }

        var mallocs = new ArrayList<Function>();
        var names = new ArrayList<String>();

        intoStream(getCurrentProgram().getFunctionManager().getFunctions(true))
                .filter(f -> f.getName().equals("malloc"))
                .forEach(mallocs::add);
        names.add("malloc");

        intoStream(getCurrentProgram().getFunctionManager().getFunctions(true))
                .filter(f -> f.getName().equals("calloc"))
                .forEach(mallocs::add);
        names.add("calloc");

        intoStream(getCurrentProgram().getFunctionManager().getFunctions(true))
                .filter(f -> f.getName().equals("__rust_alloc"))
                .forEach(mallocs::add);
        names.add("__rust_alloc");

        intoStream(getCurrentProgram().getFunctionManager().getFunctions(true))
                .filter(f -> f.getName().equals("__rdl_alloc"))
                .forEach(mallocs::add);
        names.add("__rdl_alloc");

        if (mallocs.isEmpty()) {
            this.popup("No mallocs found; failing fast.");
            return;
        }


        var base = mallocs.stream().flatMap(malloc -> intoStream(getCurrentProgram().getReferenceManager().getReferencesTo(malloc.getEntryPoint())))
                .map(Reference::getFromAddress);
        var filtered = filter ? base.filter(addr -> currentSelection.contains(addr)) : base;
        var callers = filtered.map(addr -> getCurrentProgram().getFunctionManager().getFunctionContaining(addr))
                .filter(Objects::nonNull)
                .filter(f -> !names.contains(f.getName())) // self-calls
                .collect(Collectors.toSet());

        ParallelDecompiler.decompileFunctions(new CheekyMallocCallback(getCurrentProgram(), mallocs, decomp -> decomp.toggleCCode(false)), callers, monitor);
    }

    private class CheekyMallocCallback extends DecompilerCallback<Void> {
        private final List<Function> mallocs;

        public CheekyMallocCallback(Program program, List<Function> mallocs, DecompileConfigurer configurer) {
            super(program, configurer);
            this.mallocs = mallocs;
        }

        @Override
        public Void process(DecompileResults results, TaskMonitor monitor) {
            search(mallocs, results);
            return null;
        }
    }

    private <T> Stream<T> intoStream(Iterator<T> iter) {
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(iter, 0), false);
    }

    private void search(List<Function> mallocs, DecompileResults decompileResults) {
        var sussy = new HashMap<PcodeOp, Function>();
        var high = decompileResults.getHighFunction();
        if (high == null) {
            printerr("Couldn't decompile " + decompileResults.getFunction().getName() + ", skipping");
            return;
        }

        var queue = new LinkedList<>(Stream.concat(
                        // concrete calls
                        intoStream(high.getPcodeOps())
                                .filter(op -> op.getOpcode() == PcodeOp.CALL)
                                .filter(op -> mallocs.stream().map(Function::getEntryPoint).anyMatch(addr -> op.getInput(0).getAddress().equals(addr)))
                                .map(op -> op.getInput(1)),
                        // indirect calls
                        intoStream(high.getPcodeOps()).filter(op -> op.getOpcode() == PcodeOp.CALLIND).map(ind -> {
                            var callsitePtr = ind.getInput(0);
                            var def = callsitePtr.getDef();
                            if (def != null && def.getOpcode() == PcodeOp.CAST) {
                                var actualPtr = def.getInput(0).getAddress();
                                if (actualPtr != null) {
                                    var data = currentProgram.getListing().getDefinedDataAt(actualPtr);
                                    if (data != null) {
                                        var obj = data.getValue();
                                        if (obj instanceof Address actual) {
                                            // this is a static indirect
                                            if (mallocs.stream().map(Function::getEntryPoint).anyMatch(actual::equals)) {
                                                return ind.getInput(1);
                                            }
                                        }
                                    }
                                }
                            }
                            return null;
                        }).filter(Objects::nonNull))
                .toList());

        var visited = new HashSet<Varnode>();
        while (!queue.isEmpty()) {
            var curr = queue.poll();
            if (visited.contains(curr)) {
                continue;
            }
            visited.add(curr);

            var def = curr.getDef();
            if (def == null) {
                continue;
            }
            switch (def.getOpcode()) {
                case PcodeOp.COPY -> queue.push(def.getInput(0));
                case PcodeOp.MULTIEQUAL -> queue.addAll(Arrays.asList(def.getInputs()));
                case PcodeOp.INT_ADD, PcodeOp.INT_SUB, PcodeOp.INT_LEFT, PcodeOp.INT_MULT, PcodeOp.INT_OR, PcodeOp.INT_XOR -> {
                    queue.push(def.getInput(0));
                    queue.push(def.getInput(1));
                    sussy.put(def, high.getFunction());
                }
            }
        }

        Stream.of(PcodeOp.INT_MULT, PcodeOp.INT_LEFT, PcodeOp.INT_ADD, PcodeOp.INT_SUB, PcodeOp.INT_XOR, PcodeOp.INT_OR)
                .flatMap(opcode -> sussy.entrySet().stream().filter(e -> e.getKey().getOpcode() == opcode))
                .forEach(sus -> {
                    String formatted = String.format("%s : %s -> %s", sus.getValue().getName(), sus.getKey().getSeqnum().getTarget(), sus.getKey().toString());
                    println(formatted);
                    if (currentProgram.getBookmarkManager().getBookmark(sus.getKey().getSeqnum().getTarget(), "Analysis", "Cheeky Malloc") == null) {
                        currentProgram.getBookmarkManager().setBookmark(sus.getKey().getSeqnum().getTarget(), "Analysis", "Cheeky Malloc", formatted);
                    }
                });
    }

}
