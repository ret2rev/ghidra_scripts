// Download the whole ubuntu archive as a ghidra project.
//
// NOTE: You MUST add xz to $GHIDRA_HOME/Ghidra/Features/Base/lib/ from: https://mvnrepository.com/artifact/org.tukaani/xz/1.9

import ghidra.app.script.GhidraScript;
import ghidra.formats.gfilesystem.FSRL;
import ghidra.plugins.importer.batch.BatchInfo;
import ghidra.plugins.importer.tasks.ImportBatchTask;
import ghidra.util.task.TaskMonitor;
import org.apache.commons.compress.archivers.ar.ArArchiveEntry;
import org.apache.commons.compress.archivers.ar.ArArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.compress.compressors.xz.XZCompressorInputStream;
import org.apache.commons.io.FilenameUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.stream.Collectors;

public class Crater extends GhidraScript {
    @Override
    protected void run() throws Exception {
        // update as necessary
        var mirror = "http://mirror.us.leaseweb.net/ubuntu";
        var release = "bionic";

        var client = HttpClient.newHttpClient();
        List<String> files;
        {
            var req = HttpRequest.newBuilder(URI.create(mirror + "/dists/" + release + "/main/binary-amd64/Packages.gz")).GET().build();
            var bodyHandler = HttpResponse.BodyHandlers.ofInputStream();
            try (var reader = new BufferedReader(new InputStreamReader(new GzipCompressorInputStream(client.send(req, bodyHandler).body())))) {
                files = reader.lines().filter(s -> s.startsWith("Filename: ")).map(s -> s.substring("Filename: ".length())).collect(Collectors.toList());
            }
        }

        for (var file : files) {
            if (monitor.isCancelled()) {
                return;
            }

            var req = HttpRequest.newBuilder(URI.create(mirror + "/" + file)).GET().build();
            var bodyHandler = HttpResponse.BodyHandlers.ofInputStream();
            try (var in = new ArArchiveInputStream(client.send(req, bodyHandler).body())) {
                ArArchiveEntry arEntry;
                while ((arEntry = in.getNextArEntry()) != null) {
                    if (monitor.isCancelled()) {
                        return;
                    }
                    if (arEntry.getName().equals("data.tar.xz")) {
                        var decompressed = new XZCompressorInputStream(in);
                        // write to disk
                        var temp = new File("/tmp/" + FilenameUtils.getBaseName(file));
                        decompressed.transferTo(new FileOutputStream(temp));

                        var info = new BatchInfo();
                        var fsrl = FSRL.fromString("file://" + temp.getAbsolutePath());
                        info.addFile(fsrl, monitor);

                        var task = new ImportBatchTask(info, getProjectRootFolder(), null, true, false);

                        // invoke reflectively to throw away the window creation
                        var method = ImportBatchTask.class.getDeclaredMethod("doBatchImport", TaskMonitor.class);
                        method.setAccessible(true);
                        method.invoke(task, monitor);
                        //noinspection ResultOfMethodCallIgnored
                        temp.delete();
                    }
                }
            }
        }

    }
}
