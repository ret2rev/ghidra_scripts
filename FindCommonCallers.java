// Script to find common callers of n functions and weight them by how deep the call is.

import ghidra.app.script.GhidraScript;
import ghidra.program.model.listing.Function;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class FindCommonCallers extends GhidraScript {
    @Override
    protected void run() throws Exception {
        Collection<Function> targets = intoStream(getCurrentProgram().getFunctionManager().getFunctions(currentSelection, true)).collect(Collectors.toList());

        if (targets.isEmpty()) {
            return;
        }

        MultiValuedMap<Long, Function> weighted = targets.stream().map(this::findAllCallers).reduce((map1, map2) -> {
            MultiValuedMap<Long, Function> merged = new ArrayListValuedHashMap<>();
            for (var entry1 : map1.entries()) {
                for (var entry2 : map2.entries()) {
                    if (entry1.getValue() == entry2.getValue()) {
                        merged.put(entry1.getKey() + entry2.getKey(), entry1.getValue());
                    }
                }
            }
            return merged;
        }).get();

        weighted.entries().stream()
                .sorted(Comparator.comparingLong(Map.Entry::getKey))
                .map(e -> String.format("%s - %s - weight: %d", e.getValue().getName(), e.getValue().getEntryPoint(), e.getKey()))
                .forEach(this::println);
    }

    private MultiValuedMap<Long, Function> findAllCallers(Function func) {
        Queue<Function> currQueue = new LinkedList<>();
        Queue<Function> nextQueue = new LinkedList<>();
        currQueue.add(func);
        MultiValuedMap<Long, Function> discovered = new ArrayListValuedHashMap<>();

        for (long i = 0; !currQueue.isEmpty(); i++) {
            while (!currQueue.isEmpty()) {
                Function curr = currQueue.poll();
                if (discovered.values().contains(curr)) {
                    continue;
                }
                discovered.put(i, curr);

                nextQueue.addAll(curr.getCallingFunctions(getMonitor()));
            }
            currQueue = nextQueue;
            nextQueue = new LinkedList<>();
        }

        return discovered;
    }

    private <T> Stream<T> intoStream(Iterable<T> iter) {
        return StreamSupport.stream(iter.spliterator(), false);
    }
}
