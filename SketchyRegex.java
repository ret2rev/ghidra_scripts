// Searches for uses of regex with libstdc++, libc++, or VS C++ implementations that are prone to stack overflows.

import ghidra.app.decompiler.DecompileResults;
import ghidra.app.decompiler.parallel.DecompileConfigurer;
import ghidra.app.decompiler.parallel.DecompilerCallback;
import ghidra.app.decompiler.parallel.ParallelDecompiler;
import ghidra.app.script.GhidraScript;
import ghidra.program.model.address.Address;
import ghidra.program.model.listing.Function;
import ghidra.program.model.listing.Program;
import ghidra.program.model.pcode.PcodeOp;
import ghidra.util.task.TaskMonitor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class SketchyRegex extends GhidraScript {
    @Override
    protected void run() throws Exception {
        // does our target even use regex?
        var constructors = intoStream(getCurrentProgram().getFunctionManager().getFunctions(false)).filter(f -> f.getName().equals("basic_regex") || f.getName().startsWith("basic_regex<")).filter(f -> f.getCallingConvention().hasThisPointer()).filter(f -> {
            var ns = f.getParentNamespace();
            if (ns.getName().startsWith("basic_regex")) {
                while (!ns.getParentNamespace().isGlobal()) {
                    ns = ns.getParentNamespace();
                }
                return ns.getName().equals("std");
            }
            return false;
        }).collect(Collectors.toList());

        if (!constructors.isEmpty()) {
            println("Found some regex candidates: " + constructors);
            var callers = constructors.stream().flatMap(f -> f.getCallingFunctions(this.monitor).stream().filter(caller -> {
                var ns = f.getParentNamespace();
                while (!ns.getParentNamespace().isGlobal()) {
                    ns = ns.getParentNamespace();
                }
                return !ns.getSymbol().isDescendant(caller.getParentNamespace());
            })).collect(Collectors.toList());
            if (this.monitor.isCancelled()) {
                return;
            }
            ParallelDecompiler.decompileFunctions(new RegexArgsCallback(getCurrentProgram(), decomp -> decomp.toggleCCode(false), constructors), callers, this.monitor).stream().flatMap(List::stream).forEach(regex -> {
                if (regex.expr != null) {
                    if (isOverflowProne(regex.expr)) {
                        println(String.format("Found overflow-prone regex '%s' at %s", regex.expr, regex.call));
                    } else {
                        println(String.format("Found normal regex '%s' at %s", regex.expr, regex.call));
                    }
                } else {
                    println("Found possibly user-controlled regex at " + regex.call);
                }
            });
        }
    }

    private static <T> Stream<T> intoStream(Iterable<T> iter) {
        return StreamSupport.stream(iter.spliterator(), false);
    }

    private static boolean isOverflowProne(String expr) {
        return expr.contains("){") || expr.contains(")+") || expr.contains(")*");
    }

    private static class RegexResult {
        private final String expr;
        private final Address call;

        private RegexResult(String expr, Address call) {
            this.expr = expr;
            this.call = call;
        }
    }

    private class RegexArgsCallback extends DecompilerCallback<List<RegexResult>> {

        private final List<Function> targets;

        public RegexArgsCallback(Program program, DecompileConfigurer configurer, List<Function> targets) {
            super(program, configurer);
            this.targets = targets;
        }

        @Override
        public List<RegexResult> process(DecompileResults res, TaskMonitor taskMonitor) throws Exception {
            List<RegexResult> results = new ArrayList<>();
            for (var iter = res.getHighFunction().getPcodeOps(); iter.hasNext(); ) {
                var op = iter.next();
                if (op.getOpcode() == PcodeOp.CALL) {
                    for (var target : targets) {
                        if (target.getBody().contains(op.getInput(0).getAddress())) {
                            var vn = op.getInput(2);
                            var def = vn.getDef();
                            if (def.getOpcode() == PcodeOp.COPY && def.getInput(0).isConstant()) {
                                var program = res.getFunction().getProgram();
                                var expr_off = def.getInput(0).getOffset();
                                // this is seriously the best way to do this :(
                                var expr_addr = program.getAddressFactory().getAddress("0x" + Long.toHexString(expr_off));
                                createAsciiString(expr_addr);
                                var expr_data = getDataAt(expr_addr);
                                var expr = (String) expr_data.getValue();
                                results.add(new RegexResult(expr, op.getSeqnum().getTarget()));
                            } else {
                                results.add(new RegexResult(null, op.getSeqnum().getTarget()));
                            }
                        }
                    }
                }
            }
            return results;
        }
    }
}
